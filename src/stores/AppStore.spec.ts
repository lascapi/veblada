import { setActivePinia, createPinia } from 'pinia'
import { useAppStore } from './AppStore'
import {beforeEach, describe, expect, it} from "vitest";

describe('App Store Test', () => {
  beforeEach(() => {
    setActivePinia(createPinia())
  })

  it('should be 6', () => {
    const store = useAppStore()
    expect(store.items.length).toBe(6)
  });

  it('should be one', () => {
    const store = useAppStore()
    const item = store.getItem('202205172300')
    expect(item?.title).contains('Git command', 'Parfait')
  })
})
