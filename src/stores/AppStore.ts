import {defineStore} from "pinia";
import welcomeData from '../datas/WelcomeData.json'
import {useStorage} from "@vueuse/core";

export const useAppStore = defineStore({
    id: 'AppStore',
    state: () => {
      return {
        items: useStorage('items', welcomeData)
      }
    },
    getters: {
      getItem(): any {
        return (uuid: string) => this.items.find(item => item.uuid === uuid)
      },
      getAllItems(): Array<any> {
        return this.items
      }
    },
    actions: {
      addItem(item: any) {
        this.items.push(item)
      },
      removeItem(item: any) {
        const index = this.items.findIndex(i => i.uuid === item.uuid)
        if (index > -1) {
          this.items.splice(index, 1)
        }
      }
    }
  }
)
