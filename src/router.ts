import {createWebHistory, createRouter} from 'vue-router';
import ItemsPage from "./pages/ItemsPage.vue";
import SingleItemPage from './pages/SingleItemPage.vue'
import InformationPage from './pages/InformationPage.vue'
const router = createRouter({
    history: createWebHistory(),
    routes: [
      {
        path: '/',
        name: 'home',
        component: ItemsPage
      },
      {
        path: '/items',
        name: 'items',
        component: ItemsPage
      },
      {
        path: '/item/:itemId',
        name: 'single-item',
        component: SingleItemPage
      },
      {
        path: '/information',
        name: 'information-page',
        component: InformationPage
      }
    ]
  }
)

export default router
